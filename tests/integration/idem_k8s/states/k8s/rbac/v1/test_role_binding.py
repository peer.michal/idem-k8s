import time
import uuid

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-k8s-role_binding-" + str(int(time.time())),
}
RESOURCE_TYPE = "k8s.rbac.v1.role_binding"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    metadata_name = PARAMETER["name"]
    metadata = {
        "name": metadata_name,
        "labels": {"name": "role_binding-1"},
        "annotations": {"rbac.authorization.kubernetes.io/autoupdate": "true"},
        "namespace": "default",
    }
    role_ref = {
        "api_group": "rbac.authorization.k8s.io",
        "kind": "Role",
        "name": PARAMETER["name"],
    }

    subjects = [{"kind": "ServiceAccount", "name": "default", "namespace": "default"}]
    PARAMETER["role_ref"] = role_ref
    PARAMETER["metadata"] = metadata
    PARAMETER["subjects"] = subjects

    # create k8s.rbac.v1.role_binding
    response = await hub.states.k8s.rbac.v1.role_binding.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.k8s.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-name-invalid", depends=["present"])
async def test_exec_get_invalid_role_binding(hub, ctx):
    role_binding_name = str(int(time.time()))
    ret = await hub.exec.k8s.rbac.v1.role_binding.get(
        ctx,
        name=role_binding_name,
        resource_id=str(uuid.uuid4()),
    )
    assert ret["result"]
    assert hub.tool.k8s.comment_utils.get_empty_comment(
        resource_type=RESOURCE_TYPE, name=role_binding_name
    )[0] in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-name", depends=["exec-get-by-name-invalid"])
async def test_exec_get_role_binding(hub, ctx):
    ret = await hub.exec.k8s.rbac.v1.role_binding.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        namespace="default",
    )
    assert ret["result"], ret["comment"]


@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx):
    ret = await hub.exec.k8s.rbac.v1.role_binding.list(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["exec-get-by-name"])
async def test_update(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    updated_label_name = "updated-pvc-name-" + str(int(time.time()))
    metadata = {
        "name": PARAMETER["name"],
        "labels": {"name": updated_label_name},
        "namespace": "default",
    }
    PARAMETER["metadata"] = metadata
    response = await hub.states.k8s.rbac.v1.role_binding.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.k8s.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.k8s.rbac.v1.role_binding.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert "k8s.rbac.v1.role_binding.present" in describe_response.get(
        PARAMETER["resource_id"]
    )
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("k8s.rbac.v1.role_binding.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "k8s.rbac.v1.role_binding.present"
    )
    assert described_resource


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete k8s.rbac.v1.role_binding.
    ret = await hub.states.k8s.rbac.v1.role_binding.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        metadata=PARAMETER["metadata"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    ret.get("old_state")

    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.k8s.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.k8s.rbac.v1.role_binding.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        metadata=PARAMETER["metadata"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.k8s.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="none-resource", depends=["already-absent"])
async def test_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.k8s.rbac.v1.role_binding.absent(
        ctx, name=PARAMETER["name"], metadata=PARAMETER["metadata"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.k8s.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.k8s.rbac.v1.role_binding.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            metadata=PARAMETER["metadata"],
        )
        assert ret["result"], ret["comment"]
